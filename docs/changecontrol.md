# Change control for the IGWN Icinga 2 Dashboard

## Reporting issues / feature requests {: #issue }

Any issues with, or requests for changes to the IGWN Icinda 2 Dashboard
configuration should be raised as an
[issue](https://git.ligo.org/help/user/project/issues/index.html) on the
[IGWN Icinga 2 Dashboard configuration](https://git.ligo.org/computing/monitoring/dashboard2-configuration) project:

<https://git.ligo.org/computing/monitoring/dashboard2-configuration/-/issues/new>

## Proposing changes {: #mergerequest }

All proposed changes to the IGWN Icinga 2 Dashboard configuration **must**
be made via a
[merge request](https://git.ligo.org/help/user/project/merge_requests/index.html)
on the
[IGWN Icinga 2 Dashboard configuration](https://git.ligo.org/computing/monitoring/dashboard2-configuration) project.

Details of how to propose changes as a merge request can be seen on the
project contribution guide:

[https://git.ligo.org/computing/monitoring/dashboard2-configuration/.../CONTRIBUTING.md](https://git.ligo.org/computing/monitoring/dashboard2-configuration/-/blob/main/CONTRIBUTING.md)

!!! info "If you need assistance, just ask"

    If you aren't sure where to start with making a change, or don't know
    where to store your new/updated configuration, please ask for help.

    The best place to ask is on the
    [Computing: Help](https://chat.ligo.org/ligo/channels/computing-help)
    chat channel.

## Continuous deployment {: #cicd }

Once a change is merged, the updated configuration, plugins, and scripts
are automatically deployed via a webhook.
The deployment normally occurs within 30 seconds of the push event.
