# Useful links

Other useful links that may be useful to the reader include

- <https://icinga.com/docs/icinga-2/latest/doc/01-about/>
- <https://nagios-plugins.org/doc/guidelines.html>
