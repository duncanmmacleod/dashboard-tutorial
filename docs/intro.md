# Introduction

## What is 'monitoring'? {: #what }

'Monitoring' is the process of checking the status of a system,
or system or systems, over a period of time, in order to assert
that they are providing the service for which they were designed.

In the context of scientific computing, this mainly entails probing
(testing) each system at regular intervals to assert that there are
no problems, or to alert the responsible individuals as soon as any
problems are discovered.

## Why do we need to monitor things? {: #why }

The scientific computing performed by IGWN members is highly complex,
with many interdependent systems, each of which is prone to failure
(independent of the 'skill' of the developer/maintainer/operator).

Without any monitoring, users (researchers) would be the first to
discover any problems, and would be indirectly 'responsible' for
reporting them to the responsible individual who can fix it.
Such reports may be uninformed, or vague, based on the technical
expertise/experience of the reporter, and could potentially require
significant time diagnose the root cause.

## How do we monitor things in IGWN? {: #how }

In IGWN, different groups of systems are monitored in different ways
using different technologies based on the needs and expertise of the
groups.

IGWN Computing (the combined Computing and Software groups of the
member collaborations) support a unified monitoring interface based on
[Icinga](./icinga.md) that is open for use by all collaboration members.

