# Icinga 2 Web Interface {: #web }

The main IGWN Icinga 2 Dashboard interface is visible at

<https://dashboard.igwn.org>

!!! tip "Access restricted to authorised individuals"

    Access to this site is restricted to KAGRA and LIGO.ORG identity holders.

!!! note "This overview is for desktop users"

    The remainder of this interface overview is aimed at users viewing
    the system from a desktop (or laptop) display.

    Viewers on mobile devices may experience a different layout, but the


## Navigating Icinga 2 {: #navigation }

The Icinga 2 interface has a relatively simple layout with a few main features.

![Icinga 2 left-hand navigation menu](./images/icinga2-left-navigation.png "Icinga 2 left-hand navigation menu")

### :octicons-search-16: Search {: #search }

Icinga 2 search dynamically searches (as you type) for all references to the
value give, and presents summaries of all hosts and services discovered.

### Problem count {: #problems }

The **:material-information: Problems** menu item includes a count of the current
critical, unhandled service problems.

### IGWN menu {: #igwn }

The IGWN Icinga 2 Dashboard includes a custom **IGWN** navigation menu, with
links to custom curated views.

The custom **Top-level overview** is what is displayed on the default
landing page.

!!! tip "Ask for a new custom view"

    To request a new custom view under the **IGWN** menu, please
    [open a ticket to ask for one](
    https://git.ligo.org/computing/monitoring/dashboard2-configuraiton/-/issues/new).

### Overview {: #overview }

The **:fontawesome-solid-binoculars: Overview** menu includes links to summary
views of all 'object' types in Icinga 2: hosts, services, service groups, etc.

For more details on some of these, see the [_Views_](#views) section below.

### Filtering {: #filter }

All view that display multiple hosts or services include a combined
search/filter box:

![Icinga 2 search/filter input box](./images/icinga2-search-filter.png "Icinga 2 search/filter input box")

The filter options provide an extremely powerful way to downselect what hosts
or services are displayed.
Filters are made up of three components:

1.  The filter column/variable name e.g. `Service name`
2.  The operator, e.g. `=`
3.  The filter value, which can include wildcards, e.g. `*virgo*`

!!! tip "Filter result pages are permalinked"

    Once a filter has been constructed, the URL of the result page includes
    search query parameters, so can act as a permalink to the result of that
    search.
    For example, to find all services who's name starts with `llhoft`:

    <https://dashboard.igwn.org/icingaweb2/icingadb/services?service.name=llhoft%2A>

## Views

### Default overview {: #overview }

The default view is the custom IGWN **Top-level overview** page, which
shows a custom [Icinga Cube](https://github.com/Icinga/icingaweb2-module-cube/)
overview of all production services.

[![IGWN top-level overview](./images/icinga2-top-level-overview.png "IGWN top-level overview (click for live view)")](https://dashboard.igwn.org/){: target="\_blank" }

The two sections of services are:

#### IFO status {: #ifostatus }

One cube for each detector summarises the state of that instrument as follows

- :purple_square: No information is available
- :red_square: Instrument is not operating
- :yellow_square: Instrument is locked, but not observing
- :green_square: Instrument is observing

#### Services {: #cubes }

Each cube in the **Services** section represents the state of all services
under that heading.

!!! info "Services are manually included in a cube"

    To enable this view, all services are manually configured to be in one
    (and only one) service cube.

!!! info "Only 'Production' and 'Backup' services are shown"

    The default overview page only includes services that are configured
    as `Production` or `Backup`.
    Services without a configuration, or configured using any other value
    (e.g. `Testing`) are not shown.

The colour of the cube is determined by the most 'severe' state for any
included service, meaning that if any service is `CRITICAL` and unhandled
(not acknowledged) the cube will be :red_square: red.

If the cube is not :green_square: green, it will include a set of small
squares along the bottom that summarise the count of services in other states.

### Tactical Overview {: #tactical }

The **Tactical Overview** page is a special view that presents a summary of
all hosts and services in a doughnut chart:

[![Icinga2 tactical overview](./images/icinga2-tactical-overview.png "Icinga2 tactical overview (click for live view)")](https://dashboard.igwn.org/icingaweb2/icingadb/tactical){: target="\_blank" }

### Host/service status {: #status }

#### Host view {: #host }

The overview page for a single host looks like this:

[![Icinga2 Host status](./images/icinga2-host-status.png "Icinga2 Host status (click to open live view)")](https://dashboard.igwn.org/icingaweb2/icingadb/host?name=olserver54){: target="\_blank" }

#### Service view {: #service }

The overview page for a single service looks like this:

[![Icinga2 Service status](./images/icinga2-service-status.png "Icinga2 Service status (click to open live view)")](https://dashboard.igwn.org/icingaweb2/icingadb/service?name=data-aggregation-V-HoftOnline&host.name=olserver54){: target="\_blank" }

#### Check statistics and performance data {: #data }

For both Hosts and Services, the status page includes a summary of the check
performed, including information on where and when the check was last executed,
how long it took to run, and when it will be next scheduled.

[![Icinga2 check data](./images/icinga2-check-data.png "Icinga2 check data (click to open live view for this service)")](https://dashboard.igwn.org/icingaweb2/icingadb/service?name=data-aggregation-V-HoftOnline&host.name=olserver54){: target="\_blank" }

The **Performance Data** section includes custom extra data that may be
reported by the check itself.
These data typically take the form of numerical values relative to some
threshold, e.g. fraction of available disk space, or time taken to perform
an HTTP query.

!!! note "Performance data is controlled by the plugin"

    The [performance data](https://icinga.com/docs/icinga-2/latest/doc/05-service-monitoring/#performance-data-metrics)
    is controlled entirely by the plugin used to execute the check.

!!! note "Feature request: grafana front-end to performance data"

    There is a desire in IGWN to configure
    [a Grafana instance](https://git.ligo.org/computing/monitoring/dashboard2-configuration/-/issues/13)
    to provide graphical summarise of historical performance data.

#### Custom variables {: #variables }

For both Hosts and Services, the status page includes a full listing of the
[Custom variables](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#custom-variables)
defined for that Host/Services.

[![Icinga2 custom variables](./images/icinga2-custom-variables.png "Icinga2 custom variables (click to open live view for this service)")](https://dashboard.igwn.org/icingaweb2/icingadb/service?name=data-aggregation-V-HoftOnline&host.name=olserver54){: target="\_blank" }

Such variables are typically used to customise the check that is performed,
or two apply special attributes to the object.

In IGWN, custom variables are used to insert services into cubes for the
[**Top-level overview**](#overview) page.
