# Icinga 2 {: #icinga2 }

## Icinga {: #icinga }

[Icinga](https://icinga.com/) is both a monitoring system
and the name of the company that develops and sells it.

The system, in particular, is an open-source application, originally
forked from [Nagios](https://www.nagios.org/), that provides
a scalable platform for monitoring a heterogeneous set of resources,
with a web frontend.

Icinga version 1 was basically a clone of Nagios (3?), however
version 2 includes a number of new features and a new configuration
syntax.
IGWN currently operates Icinga 2 on
[dashboard.igwn.org](https://dashboard.igwn.org/).

## Basic concepts {: #basics }

Icinga 2 is built around the basic concepts of Hosts and Services.

### Host {: #host}

A host is defined by an `address` (the name or IP of the host) and
can be in one of two states:

| Name | Description |
| ---- | ----------- |
| UP | The host is available |
| DOWN | The host is unavailable |

!!! info "Default host check is `ping`"

    By default, a host is checked via `ping`, however this can be
    customised to be basically anything.

    For example, a number of hosts running on cloud services are
    checked by testing a TCP connection over a specific port.

### Service {: #service }

A service is defined by a 'check' performed on a host and can be in any of
the following states:

| Name | Description |
| ---- | ----------- |
| OK | The service is working properly. |
| WARNING | The service is experiencing some problems but is still considered to be in working condition. |
| CRITICAL | The check successfully determined that the service is in a critical state. |
| UNKNOWN | The check could not determine the service’s state. |

The check is executed from the Icinga 2 instance, and can leverage any of the
configured commands.
Typically checks are performed over HTTP(S), but may use the special NRPE
protocol to schedule checks to be performed on a remote host (typically the
service host).

### User {: #user }

A user is defined basically by an email address that defines where notifications
should be sent.

### Group {: #group }

Hosts, Services, and Users can be in zero or more groups that enable custom
views on the web interface.

Typically hosts or services are grouped by location (e.g. 'Cascina hosts'),
or by purpose ('Low-latency data distribution services').

## Checks

The state of a Host or Service is determined by Icinga calling out to
an executable installed on the Icinga system.
These executables, also known as plugins, perform the actual check and
report their output in a standard format with the exit code indicating
the check result (OK, WARNING, etc).

For more details on check commands and plugins,
see [_Check commands_](./plugins).

## Managing a host/service {: #manage }

### Problem states {: #problems }

A Host/Service is considered in a 'problem' state when it is anything other
than 'OK'.

Icinga 2 will retry the check a few times before reporting problems; the
typical series of events, starting from an OK state, is as follows:

1.  The host/service check reports OK.

    The host/service is said to be in a `HARD`
    [`HARD`](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#hard-soft-states),
    state, meaning its state has not changed recently.

    The check is repeated every 5 minutes (according to the `check_interval`
    configuration setting).

1.  The host/service check reports a non-OK state.

    At this point the host/service enters a
    [`SOFT`](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#hard-soft-states),
    state, meaning a problem has been detected but will be rechecked.

1.  The check is retried.

    Retries will happen every 1 minute (according to the `retry_interval`
    configuration setting), up to five times (according to `max_check_attempts`).

1.  If the check recovers (reports OK) during any of these retries, the
    host/service re-enters the (`HARD`) OK state.

    !!! tip "No notifications are sent for `SOFT` state changes."

        If a check recovers from a `SOFT` problem state, no notifications
        will be sent reporting either the original problem, or the recovery.

1.  If the check fails each time, the host/service enters a
    [`HARD`](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#hard-soft-states),
    state, meaning a problem has been identified and will be reported.

    If the problem has not been acknowledged, no upstream
    [dependencies](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#dependencies)
    are also in a problem state, and no
    downtime has been scheduled, a `Problem` notification will be sent.

1.  The check repeates every 5 minutes (according to the `check_interval`).

    When the host/service check next reports OK, a `Recovery` notification
    will be sent.

### Notifications {: #notifications }

When a service enters, or recovers from, a `HARD` problem state, a
notification will be emitted.

Notifications will be sent via e-mail to all users listed in the
configuration for that host/service, and to any users in groups listed
in the configuration.

Notificiations are repeated every 1 hour until the problem is
[acknowledged](#acknowledge), recovers, or an upstream dependency fails.

### Acknowledging problems {: #acknowledge }

Host/service problems can be acknowledged to indicate that the problem
is being handled by the reponsible individual(s).

!!! info "How to acknowledge a problem"

    To acknowledge a problem, click the **:material-check-circle: Acknowledge**
    button in the quick actions section near the top of the status page.

When a problem is acknowledged, users in the notification list will receive
an `Acknowlegement` notification, and then no further `Problem` notifications.

When a host/service recovers, a `Recovery` notification will be sent, and
the acknowledgement will be cleared.

!!! tip "Post a helpful acknowledgement comment"

    When acknowledging a problem, you should **always** include a helpful
    comment, ideally including a link to an issue tracker where more details
    can be found, or a summary of what work was performed to recover the
    host/service.

### Scheduling downtime {: #downtime }

If a host/service is known to be offline in the future, for example for
maintenance work or due to an upstream network outage, a 'downtime' can
be scheduled.

!!! info "How to schedule downtime"

    To schedule downtime for a host/service, click the
    **:fontawesome-solid-plug: Downtime** button in the quick actions section
    near the top of the status page.

Scheduling downtime disables `Problem` and `Recovery` notifications for the
relevant host/service for the listed period.
When the downtime period starts a `DowntimeStart` notification is sent,
and when it ends a `DowntimeEnd` notification sent.

!!! tip "Scheduling downtime for a host **and** its services"

    When scheduling downtime for a Host, the default is to **not**
    propagate that downtime to its services, meaning notifications _may_
    still be sent if services enter `HARD` problem states during the
    downtime interval.

    To schedule downtime for all services on the host, select the
    _All Services_ option when scheduling the downtime.
