# Icinga 2 Dashboard tutorial

This site presents an overview of the IGWN Icinga 2 Dashboard instance
available at

<https://dashboard.igwn.org>

!!! warning "Incomplete and not authoritative"

    This tutorial is likely incomplete, and is certainly not an
    authoritative or official guide to Icinga 2 and its features,
    but presents the author's knowledge of the system.
