# Check Commands {: #commands }

## What is a check command {: #checkcommand }

Each `Host` or `Service` on Icinga 2 reports the output of a check via a
[`CheckCommand`](https://icinga.com/docs/icinga-2/latest/doc/05-service-monitoring/#checkcommand-definition)
object, which configures the execution of a plugin.

A number of 'standard' checks are pre-configured as part of the
[Icinga Template Library](https://icinga.com/docs/icinga-2/latest/doc/10-icinga-template-library/),
which includes detailed documentation on how to configure each check via
[custom variables](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#custom-variables).

## What is a plugin? {: #plugin }

A plugin is an executable that performs a specific check and reports
status in a standard format.
The response is then parsed by Icinga 2 to provide the output of a check.

There are a number of standard plugins provided by the
[Monitoring Plugins project](https://www.monitoring-plugins.org/), including
those to check available disk space, load, SSH, HTTP responses, etc.

In addition, there are thousands of community plugins available from sites
like [Icinga Exchange](https://exchange.icinga.com/) that cover extra services.

## Available CheckCommands {: #list }

The following check commands are available for use in
`Host` and `Service` definitions on the IGWN Icinga 2 Dashboard:

### Icinga Template Library {: #itl }

The [Icinga Template Library](https://icinga.com/docs/icinga-2/latest/doc/10-icinga-template-library/)
includes detailed documentation of how to use any pre-configured checks.

### `check-dqsegdb`

The [`check_dqsegdb`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_dqsegdb/) plugin
checks that a DQSegDB instance is responding.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `dqsegdb_host` | **Optional**. The address of the DQSegDB host. Defaults to `"$address$"`. |
| `dqsegdb_path` | **Optional**. The path on the host under which Grafana is running. Defaults to `/`. |
| `dqsegdb_auth_type` | **Optional**. The authorisation type to use when communicating with the host. Defaults to `"none"`. |
| `dqsegdb_identity_provider` {: .nowrap } | **Optional**. The name/address of the identity provider to use when authorising with X.509. Defaults to `"login.ligo.org"`. |
| `dqsegdb_keytab` | **Optional**. The path of the Kerberos Keytab to use when authorising. Defaults to [`RobotKeytab`](#iam). |
| `dqsegdb_principal` | **Optional**. The Kerberos principal to use when authorising. Defaults to [`RobotPrincipal`](#iam). |
| `dqsegdb_token_vaultserver` {: .nowrap } | **Optional**. The address of the vault server to use when generating a SciToken. Only used with `dqsegdb_auth_type = "scitoken"`. |
| `dqsegdb_token_issuer` | **Optional**. The name of the token issuer to use. Only used with `dqsegdb_auth_type = "scitoken"`. |
| `dqsegdb_token_audience` | **Optional**. The token audience. Only used with `dqsegdb_auth_type = "scitoken"`. |
| `dqsegdb_token_scope` | **Optional**. The token scope. Only used with `dqsegdb_auth_type = "scitoken"`. |
| `dqsegdb_token_role` | **Optional**. The vault name of the token role. Only used with `dqsegdb_auth_type = "scitoken"`. |
| `dqsegdb_token_credkey` | **Optional**. The key to use in the token secretpath. Only used with `dqsegdb_auth_type = "scitoken"`. |

### `check-dqsegdb-scitoken`

This check executes [`check-dqsegdb`](#check-dqsegdb) with additional
default command parameters to configure
[SciToken authorisation](https://computing.docs.ligo.org/guide/auth/scitokens/):

| Name | Description |
| ---- | ----------- |
| `dqsegdb_auth_type` | **Optional**. The authorisation type to use when communicating with the host. Defaults to `"scitoken"`. |
| `dqsegdb_keytab` | **Optional**. The path of the Kerberos Keytab to use when authorising. Defaults to [`RobotSciTokenKeytab`](#iam). |
| `dqsegdb_principal` | **Optional**. The Kerberos principal to use when authorising. Defaults to [`RobotSciTokenPrincipal`](#iam). |
| `dqsegdb_token_vaultserver` {: .nowrap } | **Optional**. The address of the vault server to use when generating a SciToken.  |
| `dqsegdb_token_issuer` | **Optional**. The name of the token issuer to use. |
| `dqsegdb_token_audience` | **Optional**. The token audience. |
| `dqsegdb_token_scope` | **Optional**. The token scope. Defaults to `"dqsegdb.read"`. |
| `dqsegdb_token_role` | **Optional**. The vault name of the token role. Defaults to [`RobotSciTokenRole`](#iam). |
| `dqsegdb_token_credkey` | **Optional**. The key to use in the token secretpath. Defaults to [`RobotSciTokenCredkey`](#iam). |

### `check-dqsegdb-x509`

!!! warning "Deprecated"

    Support for X.509 for LIGO.ORG is ending, see
    <https://git.ligo.org/groups/computing/-/epics/25> for details.

    This check is deprecated and will be removed in the near future.

This check executes [`check-dqsegdb`](#check-dqsegdb) with additional
default command parameters to configure
[X.509 authorisation](https://computing.docs.ligo.org/guide/auth/x509/):

| Name | Description |
| ---- | ----------- |
| `dqsegdb_auth_type` | **Optional**. The authorisation type to use when communicating with the host. Defaults to `"x509"`. |
| `dqsegdb_keytab` | **Optional**. The path of the Kerberos Keytab to use when authorising. Defaults to [`RobotKeytab`](#iam). |
| `dqsegdb_principal` {: .nowrap } | **Optional**. The Kerberos principal to use when authorising. Defaults to [`RobotPrincipal`](#iam). |

### `check-dqsegdb-latency`

The [`check_dqsegdb_latency`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_dqsegdb_latency/) plugin
checks the latency of the
latest `known` segment for a flag in a DQSegDB instance.

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-dqsegdb-latency-scitoken`

This check executes [`check-dqsegdb-latency`](#check-dqsegdb-latency) with additional
default command parameters to configure
[SciToken authorisation](https://computing.docs.ligo.org/guide/auth/scitokens/):

| Name | Description |
| ---- | ----------- |
| `dqsegdb_auth_type` | **Optional**. The authorisation type to use when communicating with the host. Defaults to `"scitoken"`. |
| `dqsegdb_keytab` | **Optional**. The path of the Kerberos Keytab to use when authorising. Defaults to [`RobotSciTokenKeytab`](#iam). |
| `dqsegdb_principal` | **Optional**. The Kerberos principal to use when authorising. Defaults to [`RobotSciTokenPrincipal`](#iam). |
| `dqsegdb_token_vaultserver` {: .nowrap } | **Optional**. The address of the vault server to use when generating a SciToken.  |
| `dqsegdb_token_issuer` | **Optional**. The name of the token issuer to use. |
| `dqsegdb_token_audience` {: .nowrap} | **Optional**. The token audience. |
| `dqsegdb_token_scope` | **Optional**. The token scope. Defaults to `"dqsegdb.read"`. |
| `dqsegdb_token_role` | **Optional**. The vault name of the token role. Defaults to [`RobotSciTokenRole`](#iam). |
| `dqsegdb_token_credkey` | **Optional**. The key to use in the token secretpath. Defaults to [`RobotSciTokenCredkey`](#iam). |

### `check-dqsegdb-latency-x509`

!!! warning "Deprecated"

    Support for X.509 for LIGO.ORG is ending, see
    <https://git.ligo.org/groups/computing/-/epics/25> for details.

    This check is deprecated and will be removed in the near future.

This check executes [`check-dqsegdb`](#check-dqsegdb-latency) with additional
default command parameters to configure
[X.509 authorisation](https://computing.docs.ligo.org/guide/auth/x509/):

| Name | Description |
| ---- | ----------- |
| `dqsegdb_auth_type` | **Optional**. The authorisation type to use when communicating with the host. Defaults to `"x509"`. |
| `dqsegdb_keytab` | **Optional**. The path of the Kerberos Keytab to use when authorising. Defaults to [`RobotKeytab`](#iam). |
| `dqsegdb_principal` {: .nowrap } | **Optional**. The Kerberos principal to use when authorising. Defaults to [`RobotPrincipal`](#iam). |

### `check-gettoken`

The [`check_gettoken`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_gettoken/) plugin
verifies that a new [SciToken](https://scitokens.org/) can be acquired
via a call to [`htgettoken`](https://github.com/fermitools/htgettoken).

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `gettoken_require_scope` {: .nowrap } | **Optional**. [Array](https://icinga.com/docs/icinga-2/latest/doc/17-language-reference/#array) of SciToken scopes to assert are included with the acquired SciToken. |
| `gettoken_keytab` | **Optional**. The path of the Kerberos Keytab to use when authenticating. Defaults to [`RobotSciTokenKeytab`](#iam). |
| `gettoken_principal` | **Optional**. The Kerberos principal to use when authenticating. Defaults to [`RobotSciTokenPrincipal`](#iam). |
| `gettoken_vaultserver` | **Optional**. The address of the vault server to use when acquiring the SciToken. |
| `gettoken_issuer` | **Optional**. The name of the token issuer to use. |
| `gettoken_audience` | **Optional**. The token audience. |
| `gettoken_scope` | **Optional**. The token scope. |
| `gettoken_role` | **Optional**. The vault name of the token role. Defaults to [`RobotSciTokenRole`](#iam). |
| `gettoken_credkey` | **Optional**. The key to use in the token secretpath. Defaults to [`RobotSciTokenCredkey`](#iam). |

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-gracedb`

The [`check_gracedb`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_gracedb/) plugin
checks the status of a [GraceDB](https://gracedb.ligo.org/documentation/)
instance by querying for the latest event information.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `gracedb_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`. |
| `gracedb_auth_type` | **Optional**. Authorisation type to use. Defaults to `"x509"`. |
| `gracedb_identity_provider` | **Optional**. The name/address of the IdP to use when authenticating. Defaults to `"login.ligo.org"`. |
| `gracedb_kerberos_keytab` | **Optional**. The path of the Kerberos Keytab to use generating the authorisation credential. Defaults to [`RobotKeytab`](#iam). |
| `gracedb_kerberos_principal` {: .nowrap }| **Optional**. The Kerberos principal to use when authorising with SAML or Kerberos. Defaults to [`RobotPrincipal`](#iam). |

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-grafana`

The [`check_grafana`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_grafana/) plugin
queries the [Health API](https://grafana.com/docs/grafana/latest/developers/http_api/other/#health-api) for a grafana instance and interprets the result.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `grafana_host` | **Optional**. The address of the Grafana host. Defaults to `"$address$"`. |
| `grafana_path` | **Optional**. The path on the host under which Grafana is running. Defaults to `/`. |
| `grafana_auth_type` | **Optional**. The authorisation type to use when communicating with the host. Defaults to `"any"` (try SAML and Kerberos). |
| `grafana_identity_provider` {: .nowrap } | **Optional**. The name/address of the identity provider to use when authorising with SAML. Defaults to `"login.ligo.org"`. |
| `grafana_keytab` | **Optional**. The path of the Kerberos Keytab to use when authorising with SAML or Kerberos. Defaults to [`RobotKeytab`](#iam). |
| `grafana_principal` | **Optional**. The Kerberos principal to use when authorising with SAML or Kerberos. Defaults to [`RobotPrincipal`](#iam). |

### `check-gwdatafind`

The [`check_gwdatafind`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_grafana/) plugin
checks the status of a
[GWDataFind](https://computing.docs.ligo.org/guide/data/discovery/#gwdatafind)
server by checking the
[API version](https://computing.docs.ligo.org/gwdatafind/server/api/#discovering-the-api-version-for-a-specific-server).

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `gwdatafind_host` | **Optional**. The address of the Grafana host. Defaults to `"$address$"` if the host's `address` attribute is set, `"$address6$"` otherwise |
| `gwdatafind_api_prefix` | **Optional**. The path on the host to query. |
| `gwdatafind_auth_type` | **Optional**. Authorisation type to use. Defaults to `"none"`. |
| `gwdatafind_identity_provider` | **Optional**. The name/address of the IdP to use when authenticating. Defaults to `"login.ligo.org"`. |
| `gwdatafind_kerberos_keytab` | **Optional**. The path of the Kerberos Keytab to use generating the authorisation credential. Defaults to [`RobotKeytab`](#iam). |
| `gwdatafind_kerberos_principal` {: .nowrap } | **Optional**. The Kerberos principal to use when authorising with SAML or Kerberos. Defaults to [`RobotPrincipal`](#iam). |
| `gwdatafind_token_vaultserver` | **Optional**. The address of the vault server to use when generating a SciToken. Only used with `gwdatafind_auth_type = "scitoken"`. |
| `gwdatafind_token_issuer` | **Optional**. The name of the token issuer to use. Only used with `gwdatafind_auth_type = "scitoken"`. |
| `gwdatafind_token_audience` | **Optional**. The token audience. Only used with `gwdatafind_auth_type = "scitoken"`. |
| `gwdatafind_token_scope` | **Optional**. The token scope. Only used with `gwdatafind_auth_type = "scitoken"`. |
| `gwdatafind_token_role` | **Optional**. The vault name of the token role. Only used with `gwdatafind_auth_type = "scitoken"`. |
| `gwdatafind_token_credkey` | **Optional**. The key to use in the token secretpath. Only used with `gwdatafind_auth_type = "scitoken"`. |

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-gwdatafind-scitoken`

This check executes [`check-gwdatafind`](#check-gwdatafind) with additional
default command parameters to configure
[SciToken authorisation](https://computing.docs.ligo.org/guide/auth/scitokens/).

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-gwdatafind-x509`

This check executes [`check-gwdatafind`](#check-gwdatafind) with additional
default command parameters to configure
[X.509](https://computing.docs.ligo.org/guide/auth/x509/).

!!! warning "Deprecated"

    Support for X.509 for LIGO.ORG is ending, see
    <https://git.ligo.org/groups/computing/-/epics/25> for details.

    This check is deprecated and will be removed in the near future.

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-gwdatafind-latency`

The [`check_gwdatafind_latency`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_grafana_latency/) plugin
checks the latency of the
[latest](https://computing.docs.ligo.org/gwdatafind/server/api/v1.html#latest-api)
file available for a specific dataset from a
[GWDataFind](https://computing.docs.ligo.org/guide/data/discovery/#gwdatafind) server.

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-gwdatafind-latency-scitoken`

This check executes [`check-gwdatafind-latency`](#check-gwdatafind-latency) with additional
default command parameters to configure
[SciToken authorisation](https://computing.docs.ligo.org/guide/auth/scitokens/).

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-gwdatafind-latency-x509`

This check executes [`check-gwdatafind-latency`](#check-gwdatafind-latency) with additional
default command parameters to configure
[X.509](https://computing.docs.ligo.org/guide/auth/x509/).

!!! warning "Deprecated"

    Support for X.509 for LIGO.ORG is ending, see
    <https://git.ligo.org/groups/computing/-/epics/25> for details.

    This check is deprecated and will be removed in the near future.

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-gwosc`

The [`check_gwosc`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_gwosc/) plugin
checks that a [GWOSC](https://gwosc.org) API is functional.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `gwosc_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`. |

### `check-json`

The [`check_json`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_json/) plugin
downloads a JSON file and formats its information as a plugin report.

This check is useful as a way of reporting status over HTTP for a check
performed on a secure host, or when administrator access cannot be granted
for to use
[NRPE](https://icinga.com/docs/icinga-2/latest/doc/10-icinga-template-library/#nrpe).

Details of the JSON schema that should be used can be found [here](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/json/).

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `json_url` | **Required**. The URL to query for JSON. |
| `json_auth_type` | **Optional**. Authorisation type to use when accessing JSON over HTTPS. Defaults to `"saml"`. |
| `json_identity_provider` | **Optional**. The name/address of the IdP to use when authenticating. Defaults to `"login.ligo.org"`. |
| `json_kerberos_keytab` | **Optional**. The path of the Kerberos Keytab to use generating the authorisation credential. Defaults to [`RobotKeytab`](#iam). |
| `json_kerberos_principal` {: .nowrap } | **Optional**. The Kerberos principal to use when authorising with SAML or Kerberos. Defaults to [`RobotPrincipal`](#iam). |

### `check-kdc`

The [`check_kdc`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_kdc/) plugin
checks that a Kerberos Key Distribution Center (KDC) can be used to create a
Ticket-granting ticket (TGT) for an identity.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `kdc_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`. |
| `kerberos_keytab` | **Optional**. The path of the Kerberos Keytab to use when authenticating via Kerberos. Defaults to [`RobotKeytab`](#iam) |
| `kerberos_principal` {: .nowrap } | **Optional**. The Kerberos principal to use when authenticating via Kerberos. Defaults to [`RobotPrincipal`](#iam). |

### `check-mattermost`

The [`check_mattermost`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_mattermost/) plugin
can be used to check the health of a Mattermost instance via the
[`ping` API endpoint](https://docs.mattermost.com/manage/health-checks.html).

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `mattermost_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`. |

### `check-rsync`

The [`check_rsync`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_rsync/) plugin
can be used to check an Rsync server.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `rsync_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`. |
| `rsync_port` | **Optional**. Port number. Defaults to `873`. |

### `check-scitoken-issuer`

The [`check_scitoken_issuer`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_scitoken_issuer/) plugin
can be used to check that a SciToken issuer is responding.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `token_issuer_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`. |
| `token_issuer_name` | **Required**. The name of the issuer. |

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-url`

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-url-scitokens`

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `check-vault`

The [`check_vault`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_vault/) plugin
can be used to check the health of a Hashicorp Vault instance via the
[Health API endpoint](https://developer.hashicorp.com/vault/api-docs/system/health).

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `vault_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`. |
| `vault_port` | **Optional**. Port number. Defaults to `8200`. |
| `vault_path` | **Optional**. The path on the host under which Vault is running. Defaults to `/`. |
| `vault_code` | **Optional**. The expected HTTP status code. Defaults to `200`. |

### `gitlab`

The [`check_gitlab`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_gitlab/) plugin
can be used to check that a GitLab instance is responding.

!!! info "Requires IP allowlist"

    This plugin requires unauthorised access to the
    [Health Check API](https://git.ligo.org/help/administration/monitoring/health_check.html),
    which likely requires ensuring that the IP address of the Icinga 2 instance is
    in the IP allowlist of the GitLab instance.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `gitlab_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`. |

### `koji`

The [`check_koji`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_koji/) plugin
can be used to check that a Koji instance is responding.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `koji_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`. |
| `koji_timeout` | **Optional**. Seconds before connection times out. Defaults to `10`. |

### `nds2`

The [`check_nds2`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_nds2/) plugin
connects to an NDS2 instance and executes the `count_channels()` call.

The output states are as follows:

| State | Condition |
| ----- | --------- |
| `OK`  | If the server responds with a non-zero channel count |
| `WARNING` | If the server responds with channel count of zero (0) |
| `CRITICAL` | If the server doesn't respond, or the `channel_count()` call fails |

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `nds2_host` | **Optional**. The address of the NDS2 host. Defaults to `"$address$"`. |
| `nds2_port` | **Optional**. Port number. Defaults to `31200`. |
| `nds2_auth_type` | **Optional**. The authorisation type to use when communicating with the host. Defaults to `"kerberos"` |.
| `nds2_keytab` | **Optional**. The path of the Kerberos Keytab to use when authenticating via Kerberos. Defaults to [`RobotKeytab`](#iam). |
| `nds2_principal` {: .nowrap } | **Optional**. The Kerberos principal to use when authenticating via Kerberos. Defaults to [`RobotPrincipal`](#iam). |

### `nmap`

The [`check_nmap`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_nmap/) plugin
checks the response of a host to an [Nmap](https://nmap.org/) ping scan.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `nmap_host` | **Optional**. Host name or address to query. Defaults to `"$address$"`.

### `xrootd-copy`

The [`check_xrdcp`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_xrdcp/) plugin
checks that a file can be copied from an XRootD server using `xrdcp`.

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `xrootd-copy-scitokens`

This check executes [`xrootd-copy`](#xrootd-copy) with additional
default command parameters to configure
[SciToken authorisation](https://computing.docs.ligo.org/guide/auth/scitokens/).

!!! warning "Work in progress"

    This section is a work in progress, please check back later.

### `xrootd-ping`

The [`check_xrootd_ping`](https://computing.docs.ligo.org/monitoring/igwn-monitoring-plugins/plugins/check_xrootd_ping/) plugin
checks that an XRootD server responds to `GET /ping`.

Custom variables passed as [command parameters](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#command-passing-parameters):

| Name | Description |
| ---- | ----------- |
| `xrootd_host` | **Optional**. The address of the XRootD host. Defaults to `"$address$"`. |
| `xrootd_port` | **Optional**. Port number. Defaults to `1094`. |
| `xrootd_timeout` | **Optional**. Seconds before connection times out. Defaults to `30`. |
| `xrootd_warning` | **Optional**. Response time above which to report `WARNING`. Defaults to `1.0`. |
| `xrootd_critical` {: .nowrap } | **Optional**. Response time above which to report `CRITICAL`. Defaults to `2.0`. |

## Authentication and Authorisation for plugins {: #iam }

Many IGWN services require authentication or authorisation in order
to retrieve information.
The IGWN Icinga 2 Dashboard uses two 'robot' identites (for technical
reasons) to authenticate against the LIGO.ORG Identity Provider (IdP)
in order to acquire the following authorisation credentials:

- [Kerberos TGTs](https://computing.docs.ligo.org/guide/auth/kerberos/)
- [SciTokens](https://computing.docs.ligo.org/guide/auth/scitokens/)
- [X.509 certificates](https://computing.docs.ligo.org/guide/auth/x509/)

The robot identities are made available to check commands via the
following Icinga 2 `const` variables:

| Variable | Description |
| -------- | ----------- |
| `RobotPrincipal` | The Kerberos principal name for the robot identity |
| `RobotKeytab` | The path of the keytab file for the `RobotPrincipal` |
| `RobotSciTokenCredCache` | The path of the Kerberos credential cache for the SciToken robot identity |
| `RobotSciTokenPrincipal` | The Kerberos principal name for the SciToken robot identity |
| `RobotSciTokenKeytab` | The path of the keytab file for the `RobotSciTokenPrincipal` |
| `RobotSciTokenRole` | The vault role for the SciToken robot identity |
| `RobotSciTokenCredkey` | The vault credkey for the SciToken robot identity |
| `RobotSciTokenVaultTokenFile` | The path of the vault token for the SciToken robot identity (for the production token issuer) |
| `RobotSciTokenTestVaultTokenFile` {: .nowrap } | The path of the vault token for the SciToken robot identity (for the test token issuer) |

These variables are made available to all `CheckCommand` objects to use
in order to successfully authenticate against or authorise interactions with
IGWN services.

## Adding a new plugin {: #new }

If none of the available plugins perform the right check check, you can
write one of your own.

The plugin must follow the
[Plugin API specification](https://icinga.com/docs/icinga-2/latest/doc/05-service-monitoring/#service-monitoring-plugin-api).

Python-based plugins should be integrated with the
[IGWN Monitoring Plugins](https://git.ligo.org/computing/monitoring/igwn-monitoring-plugins)
project.
These are installed on the `dashboard.igwn.org` host in the `PluginsDir`.

Standalone scripts or other plugins can be included in the
[`local-plugins/`](https://git.ligo.org/computing/monitoring/dashboard2-configuration/-/tree/main/local-plugins/)
directory of the IGWN Icinga 2 Dashboard configuration project.
These are installed on the `dashboard.igwn.org` host in the `LocalPluginsDir`.
