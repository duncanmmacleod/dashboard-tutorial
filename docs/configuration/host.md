# Host configuration {: #host }

## Basic configuration {: #basic }

Please refer to the following Icinga 2 docs pages for details on how to configure a `Host`:

- <https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#hosts-and-services>
- <https://icinga.com/docs/icinga-2/latest/doc/09-object-types/#host>

The remainder of this page describes only the IGWN-specific configuration
options.

## Custom host `vars` {: #vars }

The IGWN Icinga2 Dashboard configuration supports a few
[custom variables](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#custom-variables)
that can be applied to dynamically create `Service` definitions based on the
`Host` definition.

### `cube_name` {: #cube_name }

All `Host` definitions may (optionally) include the `cube_name` variable to
include this `Host` in the named cube on the
[Top-level overview](../interface.md#top-level-overview) page.

!!! example "Host `cube_name`"

    ```apache
    object Host "myhost" {
      import "generic-host"
      address = "myhost.example.com"
      vars.cube_name = "World domination"
    }
    ```

!!! tip "All services inherit a default `cube_name` from their `Host`"

    All `Service` objects for a `Host` will inherit a default value for
    `cube_name` from their `Host`.

### GWDataFind server types {: #gwdatafind_types }

GWDataFind server hosts can be configured with the `vars.gwdatafind_types`
custom variables as a 
[dictionary](https://icinga.com/docs/icinga-2/latest/doc/17-language-reference/#dictionary)
defining the details and expected latency of a dataset.

In addition, any host custom vars with the `gwdatafind_` prefix will be
applied to all latency checks.

!!! example "Host `gwdatafind_types` configuration"

    ```apache
    object Host "mydatafind" {
      import "datafind-host"
      vars.gwdatafind_auth_type = "scitoken"
      vars.gwdatafind_types = {
        # LIGO-Hanford
        "H1_HOFT_C00" = {
          latency_warning = 43200
          latency_critical = 86400
        }
        # LIGO-Livingston
        "L1_HOFT_C00" = {
          latency_warning = 43200
          latency_critical = 86400
        }
        # Virgo
        "HoftOnline" = {
          observatory = "V"
          latency_warning = 43200
          latency_critical = 86400
        }
      }
    }
    ```

The above configuration would define a `gwdatafind-latency-<tag>` service for
each of the listed datasets, using SciToken authorisation.

Notes:

-   Any `Host` that includes the `gwdatafind_types` variable will also
    automatically be configured with a `Service` called `gwdatafind` that
    checks the underlying GWDataFind service on that Host (via the
    `check_gwdatafind` plugin).

### HTTP(s) checks {: #http_vhosts }

`Host` definitions that include the `vars.http_vhosts` custom variable as a
[dictionary](https://icinga.com/docs/icinga-2/latest/doc/17-language-reference/#dictionary)
will dynamically configure a `Service` named according to the dictionary `key`
that executes the
[`http`](https://icinga.com/docs/icinga-2/latest/doc/10-icinga-template-library/#http)
command.

The `value` of the dictionary should itself be a `Dictionary` configuring the
HTTP check.

!!! example "Host `http_vhosts` configuration"

    ```apache
    object Host "dashboard2" {
      import "uwm-host"
      address = "dashboard.igwn.org"

      vars.http_vhosts["http"] = {
      }
      vars.http_vhosts["cert"] = {
        http_certificate = "7,1"
      }
      vars.http_vhosts["https"] = {
        http_ssl = true
      }
    }
    ```

The above `Host` configuration will dynamically include three services called
`http`, `cert`, and `https` that check functionality of HTTP with/without SSL
and assert the lifetime of the SSL/TLS certificate of the host (via HTTP).

Notes:

-   All hosts that support access via HTTP should use HTTPS and include the
    above service checks (basically verbatim).
-   `http`, `cert`, and `https` services will automatically be configured as
    interdependent (in this order).

### NRPE commands {: #nrpe_commands }

`Host` definitions may include the following custom variables to dynamically
configure services that execute the
[`nrpe`](https://icinga.com/docs/icinga-2/latest/doc/10-icinga-template-library/#nrpe)
command:

-   `nrpe_commands` - a
    [dictionary](https://icinga.com/docs/icinga-2/latest/doc/17-language-reference/#dictionary)
    of `name = command` pairs to define the name and `nrpe_command` string
    for an NRPE command.
-   `nrpe_address` - the address (if not `host.address`) of the NRPE
    endpoint to use when executing any NRPE checks.
-   `nrpe_port` - the port (if not default) to use when executing any
    NRPE checks.
-   `nrpe_timeout` - the NRPE command timeout (if not default) to use when
    executing any NRPE checks.

Notes:

-   Any `Host` that includes the `nrpe_commands` dictionary will also automatically
    be configured with a `Service` called `nrpe` that checks the basic
    functionality of the NRPE daemon on the endpoint.
-   All services configured from the `nrpe_commands` items will include a
    [Dependency](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#dependencies)
    on the `nrpe` service for the endpoint host.
This should mean that if NRPE itself stops working on the host, only the `nrpe` service will notify, and not every individual NRPE command check.


!!! example "Host `nrpe_commands` configuration"

    ```apache
    object Host "myhost" {
      import "computing-host"

      vars.nrpe_timeout = 30
      vars.nrpe_commands = {
        "disk" = "check_disk"
        "load" = "check_load"
      }
    }
    ```

With the above configuration, `myhost` will be configured to include three
(3) services as follows

| Name | Check |
| ---- | ----- |
| `nrpe` | Is NRPE functional |
| `disk` | Executes `check_disk` on the remote host |
| `load` | Executes `check_load` on the remote host |

!!! warning "Ensure that the dashboard can talk to the host on port 5666"

    In order to execute checks, the IGWN Icinga 2 Dashboard host must
    be able to communicate with the remote host over port 5666.
    This may require a custom firewall rule on the remote network.
