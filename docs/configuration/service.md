# Service configuration {: #service }

## Basic usage

Please refer to the following Icinga 2 docs pages for details on how to configure a `Service`:

- <https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#hosts-and-services>
- <https://icinga.com/docs/icinga-2/latest/doc/09-object-types/#service>

The remainder of this page describes only the IGWN-specific configuration
options.

## Custom service `vars` {: #vars }

The IGWN Icinga2 Dashboard configuration supports a few
[custom variables](https://icinga.com/docs/icinga-2/latest/doc/03-monitoring-basics/#custom-variables)
that can be applied to control the configuration of `Service` object.

!!! info "Check commands rely on custom variables"

    Check commands for a `Service` are configured using custom variables,
    check the [Commands](../commands.md) page for details of the variables
    used by builtin and IGWN custom check commands.

### `cube_name` {: #cube_name }

All `Service` definitions may (optionally) include the `cube_name` variable to
include this service in the named cube on the
[Top-level overview](../interface.md#top-level-overview) page.

!!! example "Service `cube_name` configuration"

    ```apache
    object Service "myanalysis" {
      import "awesome-science-service"
      check_command = "science_check"
      vars.cube_name = "LL Analysis"
    }
    ```
