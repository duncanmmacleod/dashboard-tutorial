# Configuring Icinga 2 for IGWN {: #configuration }

## Overview {: #overview }

The [computing/monitoring/dashboard2-configuration](https://git.ligo.org/computing/monitoring/dashboard2-configuration)
project houses the complete[^1] configuration for the Icinga 2 instance, including

- host and service configurations
- custom local plugin scripts and modules
- custom other scripts

[^1]: some of the instance configuration (web server configuration,
      security settings, etc) are not hosted here, but are handled by the host
      institution through their own configuration management setup

## Configuration Layout {: #layout }

The Icinga 2 object configuration lives in
[`config/`](https://git.ligo.org/computing/monitoring/dashboard2-configuration/-/tree/main/config/).

This directory is broken up into subdirectories for the primary working group
for the relevant service, e.g.
[`config/detchar/`](https://git.ligo.org/computing/monitoring/dashboard2-configuration/-/tree/main/detchar/)
for Detector Characterisation,
[`config/computing/`](https://git.ligo.org/computing/monitoring/dashboard2-configuration/-/tree/main/computing/)
for Computing and Software, etc.

There are also a few top-level files which configure some generic things
(e.g. the `generic-service` template), and other top-level directories which
provide common definitions (e.g. `config/commands/` defines a number of
[`CheckCommand`](https://icinga.com/docs/icinga-2/latest/doc/09-object-types/#checkcommand)
objects).

!!! info "The layout is just for humans"

    The layout of the configuration directory is technically irrelevant,
    having no bearing on the relationship between configured hosts or services.

    However, maintaining a logical hierarchy should reduce the burden of
    maintaining the complex configuration for the IGWN Icinga 2 Dashboard.
