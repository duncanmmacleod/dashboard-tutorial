# IGWN Dashboard tutorial

<https://duncanmmacleod.docs.ligo.org/dashboard-tutorial/>

This project uses [MkDocs](https://www.mkdocs.org) to present a
tutorial on the IGWN Icinga 2 Dashboard instance at

<https://dashboard.igwn.org>

See <https://git.ligo.org/duncanmmacleod/dashboard-tutorial/-/blob/main/CONTRIBUTING.md>
for details on how you can contribute to this tutorial.
